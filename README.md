# Database setup
The following procedure describes the inital setup of the any database related components.

1. Make sure you have MySQL installed (You can find more information here [MySQL](https://dev.mysql.com/downloads/installer/))
2. Execute the script `inital-setup.sql` with sufficient rights against your local MySQL instnace
3. Create a user with `All priviliges` for the newly created database
4. Change file `/src/main/resources/config.properties` to hold your newly created database user

# Program execution
1. Make sure you have Java SDK 1.8 installed (You can find more information here [Java SDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html))
2. Open this project in `Intellij` and let `Intellij` import all the `Maven` dependencies
3. Now you can start the Java program over the green play symbol as usual