-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema teko_time_reporting
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema teko_time_reporting
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `teko_time_reporting` DEFAULT CHARACTER SET utf8 ;
USE `teko_time_reporting` ;

-- -----------------------------------------------------
-- Table `teko_time_reporting`.`person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`person` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fistname` VARCHAR(150) NOT NULL,
  `lastname` VARCHAR(150) NOT NULL,
  `team_id` INT NULL,
  `isAdmin` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_person_team_idx` (`team_id` ASC),
  CONSTRAINT `fk_person_team`
    FOREIGN KEY (`team_id`)
    REFERENCES `teko_time_reporting`.`team` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`team` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `leader_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_team_person1_idx` (`leader_id` ASC),
  CONSTRAINT `fk_team_person1`
    FOREIGN KEY (`leader_id`)
    REFERENCES `teko_time_reporting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`project` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`time_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`time_account` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `max_hours` DECIMAL(10,0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`function`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`function` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`project_has_person`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`project_has_person` (
  `person_id` INT NOT NULL,
  `project_id` INT NOT NULL,
  `function_id` INT NOT NULL,
  PRIMARY KEY (`person_id`, `project_id`, `function_id`),
  INDEX `fk_person_has_project_project1_idx` (`project_id` ASC),
  INDEX `fk_person_has_project_person1_idx` (`person_id` ASC),
  INDEX `fk_team_has_person_functions1_idx` (`function_id` ASC),
  CONSTRAINT `fk_person_has_project_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `teko_time_reporting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_project_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `teko_time_reporting`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_team_has_person_functions1`
    FOREIGN KEY (`function_id`)
    REFERENCES `teko_time_reporting`.`function` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`activity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `cost_hour` DECIMAL(10,0) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `person_id` INT NOT NULL,
  `project_id` INT NOT NULL,
  `time_account_id` INT NOT NULL,
  `activity_id` INT NOT NULL,
  `date_from` DATETIME NOT NULL,
  `date_to` DATETIME NOT NULL,
  INDEX `fk_person_has_time_account_time_account1_idx` (`time_account_id` ASC),
  INDEX `fk_person_has_time_account_person1_idx` (`person_id` ASC),
  INDEX `fk_person_has_time_account_activity1_idx` (`activity_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_booking_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_person_has_time_account_person1`
    FOREIGN KEY (`person_id`)
    REFERENCES `teko_time_reporting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_time_account_time_account1`
    FOREIGN KEY (`time_account_id`)
    REFERENCES `teko_time_reporting`.`time_account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_time_account_activity1`
    FOREIGN KEY (`activity_id`)
    REFERENCES `teko_time_reporting`.`activity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `teko_time_reporting`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`report` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` DATE NOT NULL,
  `revoked` TINYINT(1) NOT NULL DEFAULT 0,
  `revoked_date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`report_booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`report_booking` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `person_id` INT NOT NULL,
  `project_id` INT NOT NULL,
  `time_account_id` INT NOT NULL,
  `activity_id` INT NOT NULL,
  `date_from` DATETIME NOT NULL,
  `date_to` DATETIME NOT NULL,
  `report_id` INT NOT NULL,
  INDEX `fk_person_has_time_account_time_account1_idx` (`time_account_id` ASC),
  INDEX `fk_person_has_time_account_person1_idx` (`person_id` ASC),
  INDEX `fk_person_has_time_account_activity1_idx` (`activity_id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_booking_copy1_report1_idx` (`report_id` ASC),
  INDEX `fk_report_booking_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_person_has_time_account_person10`
    FOREIGN KEY (`person_id`)
    REFERENCES `teko_time_reporting`.`person` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_time_account_time_account10`
    FOREIGN KEY (`time_account_id`)
    REFERENCES `teko_time_reporting`.`time_account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_person_has_time_account_activity10`
    FOREIGN KEY (`activity_id`)
    REFERENCES `teko_time_reporting`.`activity` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_copy1_report1`
    FOREIGN KEY (`report_id`)
    REFERENCES `teko_time_reporting`.`report` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_report_booking_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `teko_time_reporting`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `teko_time_reporting`.`project_has_time_account`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`project_has_time_account` (
  `project_id` INT NOT NULL,
  `time_account_id` INT NOT NULL,
  PRIMARY KEY (`project_id`, `time_account_id`),
  INDEX `fk_project_has_time_account_time_account1_idx` (`time_account_id` ASC),
  INDEX `fk_project_has_time_account_project1_idx` (`project_id` ASC),
  CONSTRAINT `fk_project_has_time_account_project1`
    FOREIGN KEY (`project_id`)
    REFERENCES `teko_time_reporting`.`project` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_project_has_time_account_time_account1`
    FOREIGN KEY (`time_account_id`)
    REFERENCES `teko_time_reporting`.`time_account` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `teko_time_reporting` ;

-- -----------------------------------------------------
-- Placeholder table for view `teko_time_reporting`.`vw_reporting`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `teko_time_reporting`.`vw_reporting` (`Team` INT, `Firstname` INT, `Lastname` INT, `Project` INT, `Time account` INT, `Activity` INT, `Date from` INT, `Date to` INT, `report_id` INT);

-- -----------------------------------------------------
-- View `teko_time_reporting`.`vw_reporting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `teko_time_reporting`.`vw_reporting`;
USE `teko_time_reporting`;
CREATE  OR REPLACE VIEW `vw_reporting` AS
SELECT
	te.name AS Team,
	pe.fistname AS Firstname,
	pe.lastname AS Lastname,
	pr.name AS Project,
	ta.name AS `Time account`,
	ac.name AS Activity,
	bo.date_from AS `Date from`,
	bo.date_to AS `Date to`,
    bo.report_id
FROM report_booking AS bo
	INNER JOIN person AS pe ON bo.person_id=pe.id
	LEFT JOIN team AS te ON pe.team_id=te.id
	INNER JOIN project AS pr ON bo.project_id=pr.id
	INNER JOIN time_account AS ta ON bo.time_account_id=ta.id
	INNER JOIN activity AS ac ON bo.activity_id=ac.id
ORDER BY te.id, pe.fistname, pe.lastname;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `teko_time_reporting`.`person`
-- -----------------------------------------------------
START TRANSACTION;
USE `teko_time_reporting`;
INSERT INTO `teko_time_reporting`.`person` (`id`, `fistname`, `lastname`, `team_id`, `isAdmin`) VALUES (1, 'Administator', 'Admin', NULL, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `teko_time_reporting`.`function`
-- -----------------------------------------------------
START TRANSACTION;
USE `teko_time_reporting`;
INSERT INTO `teko_time_reporting`.`function` (`id`, `name`) VALUES (1, 'Projektleiter');
INSERT INTO `teko_time_reporting`.`function` (`id`, `name`) VALUES (2, 'Entwickler');
INSERT INTO `teko_time_reporting`.`function` (`id`, `name`) VALUES (3, 'Designer');
INSERT INTO `teko_time_reporting`.`function` (`id`, `name`) VALUES (4, 'Verkäufer');
INSERT INTO `teko_time_reporting`.`function` (`id`, `name`) VALUES (5, 'Architekt');

COMMIT;


-- -----------------------------------------------------
-- Data for table `teko_time_reporting`.`activity`
-- -----------------------------------------------------
START TRANSACTION;
USE `teko_time_reporting`;
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (1, 'PM Level 1', 100);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (2, 'PM Level 2', 140);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (3, 'ENT Level 1', 80);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (4, 'ENT Level 2', 120);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (5, 'ENT Level 3', 160);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (6, 'DES Level 1', 140);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (7, 'SAL Level 1', 160);
INSERT INTO `teko_time_reporting`.`activity` (`id`, `name`, `cost_hour`) VALUES (8, 'ARC Level 1', 180);

COMMIT;

