package ch.teko.utils;

import ch.teko.dao.impl.MySqlProjectDAO;
import ch.teko.dao.impl.MySqlTeamDAO;
import ch.teko.model.Person;

public class UserManager {

    private static Person person;

    public static void setCurrentPerson(Person person) {
        UserManager.person = person;
    }

    public static Person getCurrentPerson() {
        return UserManager.person;
    }

    public static Boolean isCurrentUserAdmin() {
        return UserManager.person.isAdmin();
    }

    public static Boolean isProjectLead(Integer projectId) {
        return MySqlProjectDAO.getInstance().isPersonProjectLead(projectId, UserManager.person.getId());
    }

    public static Boolean isTeamLead(Integer teamId) {
        return MySqlTeamDAO.getInstance().isPersonTeamLead(teamId, UserManager.person.getId());
    }

}
