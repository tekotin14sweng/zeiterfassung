package ch.teko.utils;

import ch.teko.dao.impl.MySqlReportingDAO;
import ch.teko.model.PdfBooking;
import ch.teko.model.Report;
import com.itextpdf.io.font.FontConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.io.IOException;
import java.util.List;

public class PdfGenerator {

    public static void generatePdf(String destinationFile, Report report) {
        try {
            new PdfGenerator().createPdf(destinationFile, report);
        }
        catch (IOException e) {
            System.out.printf(e.toString());
        }
    }

    public void createPdf(String dest, Report report) throws IOException {
        //Initialize PDF writer
        PdfWriter writer = new PdfWriter(dest);

        //Initialize PDF document
        PdfDocument pdf = new PdfDocument(writer);

        // Initialize document
        Document document = new Document(pdf, PageSize.A4.rotate());

        // Add paragraph to the document
        document.add(new Paragraph("Monat: " + report.toString()));

        // Check archived
        if (report.getRevoked()) {
            document.add(new Paragraph("Archiviert seit: " + report.getRevokedDescription()));
        }

        document.add(new Paragraph(""));

        // Add table
        List<PdfBooking> bookingList = MySqlReportingDAO.getInstance().getAllPdfBooking(report.getId());
        PdfFont font = PdfFontFactory.createFont(FontConstants.HELVETICA);
        PdfFont bold = PdfFontFactory.createFont(FontConstants.HELVETICA_BOLD);
        Table table = new Table(new float[]{4, 2, 2, 2, 2, 2, 2});
        table.setWidthPercent(100);

        table.addHeaderCell(new Cell().add(new Paragraph("Zeit").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Vorname").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Nachname").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Team").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Zeitkonto").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Leistungsart").setFont(bold)));
        table.addHeaderCell(new Cell().add(new Paragraph("Projekt").setFont(bold)));

        for (PdfBooking booking: bookingList) {
            table.addCell(new Cell().add(new Paragraph(booking.getDateFromTo()).setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getFirstname()).setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getLastname()).setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getTeam() != null ? booking.getTeam(): "").setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getTimeAccount()).setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getActivity()).setFont(font)));
            table.addCell(new Cell().add(new Paragraph(booking.getProject()).setFont(font)));
        }

        document.add(table);

        //Close document
        document.close();
    }

}
