package ch.teko.utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public final class ConnectionManager {
	private static ConnectionManager instance;
	private static final Properties properties = new Properties();

	private ConnectionManager() {
		try {
			ConnectionManager.properties.load(ConnectionManager.class.getResourceAsStream("/config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Connection createConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					ConnectionManager.properties.getProperty("connectionString"),
					ConnectionManager.properties.getProperty("connectionUser"),
					ConnectionManager.properties.getProperty("connectionPwd")
			);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	public static Connection getConnection() {
		return ConnectionManager.getInstance().createConnection();
	}

	public static ConnectionManager getInstance() {
		if (ConnectionManager.instance == null) ConnectionManager.instance = new ConnectionManager();
		return ConnectionManager.instance;
	}
}
