package ch.teko.model;

import java.util.Objects;

public class Person {
	private final Integer id;
	private String firstname;
	private String lastname;
	private final Team team;
	private final Boolean isAdmin;
	private final Function function;

	public Person(
			Integer id,
			String firstname,
			String lastname,
			Team team,
			Boolean isAdmin
	) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.team = team;
		this.isAdmin = isAdmin;
		this.function = null;
	}

	public Person(
			Integer id,
			String firstname,
			String lastname,
			Team team,
			Boolean isAdmin,
			Function function
	) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.team = team;
		this.isAdmin = isAdmin;
		this.function = function;
	}

	public Person() {
		this.id = null;
		this.team = null;
		this.isAdmin = null;
		this.function = null;
	}

	public Integer getId() {
		return this.id;
	}

	public String getFirstname() {
		return this.firstname;
	}

	public String getLastname() {
		return this.lastname;
	}

	public String getFullname() { return this.firstname + " " + this.lastname; }

	public Function getFunction() {
		return this.function;
	}

	public String toString() {
		return this.firstname + " " + this.lastname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Team getTeam() {
		return this.team;
	}

	public Boolean isAdmin() {
		return isAdmin;
	}

	public boolean equals(Object o) {
		// self check
		if (this == o)
			return true;
		// null check
		if (o == null)
			return false;
		// type check and cast
		if (getClass() != o.getClass())
			return false;
		Person person = (Person) o;
		// field comparison
		return Objects.equals(this.id, person.id);
	}
}
