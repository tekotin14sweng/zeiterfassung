package ch.teko.model;

import java.util.ArrayList;
import java.util.List;

public class TimeAccount {
	private final Integer id;
	private String name;
	private Integer maxHours;
	private final List<Project> projects = new ArrayList<>();

	public TimeAccount(
			Integer id,
			String name,
			Integer maxHours
	) {
		this.id = id;
		this.name = name;
		this.maxHours = maxHours;
	}

	public TimeAccount() {
		this.id = null;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public Integer getMaxHours() {
		return this.maxHours;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setMaxHours(Integer maxHours) {
		this.maxHours = maxHours;
	}

	public String toString() { return this.getName(); }

	public void addProject(Project project) {
		this.projects.add(project);
	}

	public String getProjects() {
		String stringList = new String();
		for (Project project: this.projects) {
			stringList += project.toString() + "\n";
		}
		return stringList;
	}
}
