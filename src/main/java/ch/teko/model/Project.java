package ch.teko.model;

import java.util.ArrayList;
import java.util.List;

public class Project {
	private final Integer id;
	private String name;
	private final List<Person> projectMembers = new ArrayList<>();

	public Project(
			Integer id,
			String name
	) {
		this.id = id;
		this.name = name;
	}

	public Project(){
		this.id = null;
		this.name = null;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() { return this.name; }

	public String getProjectMembers() {
		String stringList = new String();
		for (Person person: this.projectMembers) {
			stringList += person.toString() + " (" + person.getFunction() + ")\n";
		}
		return stringList;
	}

	public void addPerson(Person person) {
		this.projectMembers.add(person);
	}
}
