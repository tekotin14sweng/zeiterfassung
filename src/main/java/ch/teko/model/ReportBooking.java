package ch.teko.model;

import java.time.LocalDateTime;

public class ReportBooking extends Booking {
	private final Integer reportId;

	public ReportBooking(
			Integer id,
			Person person,
			Project project,
			TimeAccount timeAccount,
			Activity activity,
			LocalDateTime dateFrom,
			LocalDateTime dateTo,
			Integer reportId
	) {
		super(id, person, project, timeAccount, activity, dateFrom, dateTo, null);
		this.reportId = reportId;
	}

	public Integer getReportId() {
		return this.reportId;
	}
}
