package ch.teko.model;

public class Activity {
	private final Integer id;
	private final String name;
	private final Integer costHours;

	public Activity(
			Integer id,
			String name,
			Integer costHours
	) {
		this.id = id;
		this.name = name;
		this.costHours = costHours;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public Integer getCostHours() {
		return this.costHours;
	}

	public String toString() { return this.getName(); }
}
