package ch.teko.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Booking {
	private final Integer id;
	private Person person;
	private Project project;
	private TimeAccount timeAccount;
	private Activity activity;
	private LocalDateTime dateFrom;
	private LocalDateTime dateTo;
	private Boolean archived;

	public Booking(
			Integer id,
			Person person,
			Project project,
			TimeAccount timeAccount,
			Activity activity,
			LocalDateTime dateFrom,
			LocalDateTime dateTo,
			Boolean archived
	) {
		this.id = id;
		this.person = person;
		this.project = project;
		this.timeAccount = timeAccount;
		this.activity = activity;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
		this.archived = archived;
	}

	public Booking() {
		this.id = null;
	}

	public Integer getId() {
		return this.id;
	}

	public LocalDateTime getDateFrom() {
		return this.dateFrom;
	}

	public LocalDateTime getDateTo() {
		return this.dateTo;
	}

	public Person getPerson() {
		return this.person;
	}

	public Project getProject() {
		return this.project;
	}

	public TimeAccount getTimeAccount() {
		return this.timeAccount;
	}

	public Activity getActivity() {
		return this.activity;
	}

	public String getBookingName() {
		return this.getDateFrom().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")) + " - " + this.getDateTo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")) + " / " + this.getPerson().getFullname() + " / " + this.getProject().getName() + " / " + this.getTimeAccount().getName() + " (" + this.getActivity().getName() + ")";
	}

	public Boolean getArchived() {
		return archived;
	}

	public String getArchivedText() {
		return this.getArchived() ? "Archviert" : "";
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public void setTimeAccount(TimeAccount timeAccount) {
		this.timeAccount = timeAccount;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public void setDateFrom(LocalDateTime dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setDateTo(LocalDateTime dateTo) {
		this.dateTo = dateTo;
	}
}
