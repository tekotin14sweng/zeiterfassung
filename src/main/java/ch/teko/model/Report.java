package ch.teko.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Report {
	private final Integer id;
	private final LocalDate date;
	private final Boolean revoked;
	private final LocalDate revokedDate;

	public Report(
			Integer id,
			LocalDate date,
			Boolean revoked,
			LocalDate revokedDate
	) {
		this.id = id;
		this.date = date;
		this.revoked = revoked;
		this.revokedDate = revokedDate;
	}

	public Report() {
		this.id = null;
		this.date = null;
		this.revoked = null;
		this.revokedDate = null;
	}

	public Integer getId() {
		return id;
	}

	public LocalDate getDate() {
		return this.date;
	}

	public Boolean getRevoked() {
		return this.revoked;
	}

	public LocalDate getRevokedDate() {
		return this.revokedDate;
	}

	public String toString() {
		return this.date.format(DateTimeFormatter.ofPattern("MM.yyyy"));
	}

	public String getRevokedDescription() {
		return this.revoked ? this.getRevokedDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")) : "";
	}
}
