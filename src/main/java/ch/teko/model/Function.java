package ch.teko.model;

public class Function {
	private final Integer id;
	private final String name;

	public Function(
			Integer id,
			String name
	) {
		this.id = id;
		this.name = name;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String toString() {
		return this.getName();
	}
}
