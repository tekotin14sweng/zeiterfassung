package ch.teko.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PdfBooking {
	private final String team;
	private final String firstname;
	private final String lastname;
	private final String project;
	private final String timeAccount;
	private final String activity;
	private final LocalDateTime dateFrom;
	private final LocalDateTime dateTo;

	public PdfBooking(String team, String firstname, String lastname, String project, String timeAccount, String activity, LocalDateTime dateFrom, LocalDateTime dateTo) {
		this.team = team;
		this.firstname = firstname;
		this.lastname = lastname;
		this.project = project;
		this.timeAccount = timeAccount;
		this.activity = activity;
		this.dateFrom = dateFrom;
		this.dateTo = dateTo;
	}

	public String getTeam() {
		return team;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getProject() {
		return project;
	}

	public String getTimeAccount() {
		return timeAccount;
	}

	public String getActivity() {
		return activity;
	}

	public LocalDateTime getDateFrom() {
		return dateFrom;
	}

	public LocalDateTime getDateTo() {
		return dateTo;
	}

	public String getDateFromTo() {
		return this.getDateFrom().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")) + " - " + this.getDateTo().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
	}
}
