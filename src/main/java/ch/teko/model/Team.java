package ch.teko.model;

import java.util.Objects;

public class Team {
	private final Integer id;
	private String name;
	private final Person leader;

	public Team(
			Integer id,
			String name,
			Person leader
	) {
		this.id = id;
		this.name = name;
		this.leader = leader;
	}

	public Team() {
		this.id = null;
		this.name = null;
		this.leader = null;
	}

	public Integer getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getLeaderName() {
		return this.leader.getFullname();
	}

	public Person getLeader() { return this.leader; }

	public void setName(String name) {
		this.name = name;
	}

	public String toString() { return this.name; }

	public boolean equals(Object o) {
		// self check
		if (this == o)
			return true;
		// null check
		if (o == null)
			return false;
		// type check and cast
		if (getClass() != o.getClass())
			return false;
		Team team = (Team) o;
		// field comparison
		return Objects.equals(this.id, team.id);
	}
}
