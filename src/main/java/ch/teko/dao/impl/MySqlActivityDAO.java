package ch.teko.dao.impl;

import ch.teko.dao.inter.ActivityDAO;
import ch.teko.model.Activity;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlActivityDAO implements ActivityDAO {

    private static ActivityDAO instance;

    public static ActivityDAO getInstance() {
        if (MySqlActivityDAO.instance == null) MySqlActivityDAO.instance = new MySqlActivityDAO();
        return MySqlActivityDAO.instance;
    }

    @Override
    public List<Activity> getAll() {
        List<Activity> tempList = new ArrayList<>();

        String query = new String("select * from activity;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    private Activity getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Activity(
                rs.getInt(1),
                rs.getString(2),
                rs.getInt(3)
        );
    }
}
