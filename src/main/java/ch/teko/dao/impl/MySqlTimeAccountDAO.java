package ch.teko.dao.impl;

import ch.teko.dao.inter.TimeAccountDAO;
import ch.teko.model.Project;
import ch.teko.model.TimeAccount;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySqlTimeAccountDAO implements TimeAccountDAO {

    private static TimeAccountDAO instance;

    public static TimeAccountDAO getInstance() {
        if (MySqlTimeAccountDAO.instance == null) MySqlTimeAccountDAO.instance = new MySqlTimeAccountDAO();
        return MySqlTimeAccountDAO.instance;
    }

    @Override
    public List<TimeAccount> getAll() {
        List<TimeAccount> timeAccountList = new ArrayList<>();

        String query = new String("select ta.id, ta.name, ta.max_hours, p.* from time_account as ta left join project_has_time_account as pt ON pt.time_account_id=ta.id left join project as p ON pt.project_id=p.id;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            timeAccountList = this.resultToModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timeAccountList;
    }

    @Override
    public void save(TimeAccount timeAccount) {
        String query = null;

        if (timeAccount.getId() == null) {
            query = "INSERT INTO time_account (name, max_hours) VALUES (?, ?);";
        } else {
            query = "UPDATE time_account SET name=?, max_hours=? WHERE id=?;";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setString(1, timeAccount.getName());
            statement.setInt(2, timeAccount.getMaxHours());
            if (timeAccount.getId() != null) {
                statement.setInt(3, timeAccount.getId());
            }

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        String query = new String("DELETE FROM time_account WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addProject(TimeAccount timeAccount, Integer projectId) {
        if (!this.projectExists(timeAccount, projectId)) {
            String query = "INSERT INTO project_has_time_account (time_account_id, project_id) VALUES (?, ?);";

            try (
                    Connection conn = ConnectionManager.getConnection();
                    PreparedStatement statement = conn.prepareStatement(query)
            ) {
                statement.setInt(1, timeAccount.getId());
                statement.setInt(2, projectId);

                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteProject(TimeAccount timeAccount, Integer projectId) {
        if (this.projectExists(timeAccount, projectId)) {
            String query = new String("DELETE FROM project_has_time_account WHERE time_account_id=? AND project_id=?;");

            try (
                    Connection conn = ConnectionManager.getConnection();
                    PreparedStatement statement = conn.prepareStatement(query)
            ) {
                statement.setInt(1, timeAccount.getId());
                statement.setInt(2, projectId);
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<TimeAccount> getTimeAccountByProjectId(Integer projectId) {
        List<TimeAccount> timeAccountList = new ArrayList<>();

        String query = new String("select ta.id, ta.name, ta.max_hours, p.* from time_account as ta left join project_has_time_account as pt ON pt.time_account_id=ta.id left join project as p ON pt.project_id=p.id WHERE pt.project_id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, projectId);
            ResultSet rs = statement.executeQuery();
            timeAccountList = this.resultToModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return timeAccountList;
    }

    private boolean projectExists(TimeAccount timeAccount, Integer projectId) {
        String query = new String("SELECT * FROM project_has_time_account WHERE time_account_id=? AND project_id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, timeAccount.getId());
            statement.setInt(2, projectId);
            ResultSet rs = statement.executeQuery();
            return (Boolean)rs.first();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    private List<TimeAccount> resultToModel(ResultSet rs) {
        HashMap<Integer, TimeAccount> timeAccountList = new HashMap<>();

        try {
            while (rs.next()) {
                TimeAccount timeAccount = timeAccountList.get(rs.getInt(1));
                if (timeAccount == null) {
                    timeAccountList.put(rs.getInt(1), this.getModelFromResultSet(rs));
                    if (rs.getInt(4) != 0) {
                        timeAccount = timeAccountList.get(rs.getInt(1));
                        timeAccount.addProject(new Project(
                                rs.getInt(4),
                                rs.getString(5)
                        ));
                    }
                } else {
                    timeAccount.addProject(new Project(
                            rs.getInt(4),
                            rs.getString(5)
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>(timeAccountList.values());
    }

    private TimeAccount getModelFromResultSet(ResultSet rs) throws SQLException {
        return new TimeAccount(
                rs.getInt(1),
                rs.getString(2),
                rs.getInt(3)
        );
    }
}
