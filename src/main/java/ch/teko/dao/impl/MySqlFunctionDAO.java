package ch.teko.dao.impl;

import ch.teko.dao.inter.FunctionDAO;
import ch.teko.model.Function;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlFunctionDAO implements FunctionDAO {

    private static FunctionDAO instance;

    public static FunctionDAO getInstance() {
        if (MySqlFunctionDAO.instance == null) MySqlFunctionDAO.instance = new MySqlFunctionDAO();
        return MySqlFunctionDAO.instance;
    }

    @Override
    public List<Function> getAll() {
        List<Function> tempList = new ArrayList<>();

        String query = new String("select * from function;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    private Function getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Function(
                rs.getInt(1),
                rs.getString(2)
        );
    }
}
