package ch.teko.dao.impl;

import ch.teko.dao.inter.BookingDAO;
import ch.teko.model.*;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MySqlBookingDAO implements BookingDAO {

    private static BookingDAO instance;

    public static BookingDAO getInstance() {
        if (MySqlBookingDAO.instance == null) MySqlBookingDAO.instance = new MySqlBookingDAO();
        return MySqlBookingDAO.instance;
    }

    @Override
    public List<Booking> getAll() {
        List<Booking> tempList = new ArrayList<>();

        String query = new String("select bo.*, pe.*, pr.*, ta.*, ac.*, (SELECT 1 FROM report AS re WHERE DATE_FORMAT(re.date, \"%m.%Y\") = DATE_FORMAT(bo.date_from, \"%m.%Y\") AND re.revoked=false LIMIT 1) FROM booking as bo " +
                "inner join person as pe ON bo.person_id=pe.id " +
                "inner join project as pr ON bo.project_id=pr.id " +
                "inner join time_account as ta ON bo.time_account_id=ta.id " +
                "inner join activity as ac ON bo.activity_id=ac.id;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public List<Booking> getAllByPerson(Integer personId) {
        List<Booking> tempList = new ArrayList<>();

        String query = new String("select bo.*, pe.*, pr.*, ta.*, ac.*, (SELECT 1 FROM report AS re WHERE DATE_FORMAT(re.date, \"%m.%Y\") = DATE_FORMAT(bo.date_from, \"%m.%Y\") AND re.revoked=false LIMIT 1) FROM booking as bo " +
                "inner join person as pe ON bo.person_id=pe.id " +
                "inner join project as pr ON bo.project_id=pr.id " +
                "inner join time_account as ta ON bo.time_account_id=ta.id " +
                "inner join activity as ac ON bo.activity_id=ac.id WHERE pe.id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, personId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public void save(Booking booking) {
        String query = null;

        if (booking.getId() == null) {
            query = "INSERT INTO booking (person_id, project_id, time_account_id, activity_id, date_from, date_to) VALUES (?, ?, ?, ?, ?, ?);";
        } else {
            query = "UPDATE booking SET person_id=?, project_id=?, time_account_id=?, activity_id=?, date_from=?, date_to=? WHERE id=?;";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, booking.getPerson().getId());
            statement.setInt(2, booking.getProject().getId());
            statement.setInt(3, booking.getTimeAccount().getId());
            statement.setInt(4, booking.getActivity().getId());
            statement.setTimestamp(5, java.sql.Timestamp.valueOf(booking.getDateFrom()));
            statement.setTimestamp(6, java.sql.Timestamp.valueOf(booking.getDateTo()));
            if (booking.getId() != null) {
                statement.setInt(7, booking.getId());
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        String query = new String("DELETE FROM booking WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isMonthBookable(LocalDateTime date) {
        String query = new String("SELECT 1 FROM report WHERE DATE_FORMAT(`date`, \"%m.%Y\")=? AND revoked=false;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM.yyyy");
            statement.setString(1, date.format(formatter));
            ResultSet rs = statement.executeQuery();
            return !(Boolean)rs.first();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private Booking getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Booking(
                rs.getInt(1),
                new Person(
                        rs.getInt(8),
                        rs.getString(9),
                        rs.getString(10),
                        null,
                        false
                ),
                new Project(
                        rs.getInt(13),
                        rs.getString(14)
                ),
                new TimeAccount(
                        rs.getInt(15),
                        rs.getString(16),
                        rs.getInt(17)
                ),
                new Activity(
                        rs.getInt(18),
                        rs.getString(19),
                        rs.getInt(20)
                ),
                rs.getTimestamp(6).toLocalDateTime(),
                rs.getTimestamp(7).toLocalDateTime(),
                rs.getBoolean(21)
        );
    }
}
