package ch.teko.dao.impl;

import ch.teko.dao.inter.PersonDAO;
import ch.teko.model.Person;
import ch.teko.model.Team;
import ch.teko.utils.ConnectionManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlPersonDAO implements PersonDAO {

    private static PersonDAO instance;

    public static PersonDAO getInstance() {
        if (MySqlPersonDAO.instance == null) MySqlPersonDAO.instance = new MySqlPersonDAO();
        return MySqlPersonDAO.instance;
    }

    @Override
    public List<Person> getAll() {
        List<Person> tempList = new ArrayList<>();

        String query = new String("select * from person left join team on person.team_id=team.id;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public void save(Person person, Integer teamId) {
        String query = null;

        if (person.getId() == null) {
            query = "INSERT INTO person (fistname, lastname, team_id) VALUES (?, ?, ?);";
        } else {
            query = "UPDATE person SET fistname=?, lastname=?, team_id=? WHERE id=?;";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setString(1, person.getFirstname());
            statement.setString(2, person.getLastname());
            if (teamId == 0) {
                statement.setNull(3, Types.VARCHAR);
            } else {
                statement.setInt(3, teamId);
            }
            if (person.getId() != null) {
                statement.setInt(4, person.getId());
            }

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        String query = new String("DELETE FROM person WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Person getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Person(
                rs.getInt(1),
                rs.getString(2),
                rs.getString(3),
                new Team(
                        rs.getInt(6),
                        rs.getString(7),
                        null
                ),
                rs.getBoolean(5)
        );
    }
}
