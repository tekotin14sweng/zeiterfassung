package ch.teko.dao.impl;

import ch.teko.dao.inter.ProjectDAO;
import ch.teko.model.Function;
import ch.teko.model.Person;
import ch.teko.model.Project;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MySqlProjectDAO implements ProjectDAO {

    private static ProjectDAO instance;

    public static ProjectDAO getInstance() {
        if (MySqlProjectDAO.instance == null) MySqlProjectDAO.instance = new MySqlProjectDAO();
        return MySqlProjectDAO.instance;
    }

    @Override
    public List<Project> getAll() {
        List<Project> projectList = new ArrayList<>();

        String query = new String("select p.id, p.name, ps.*, f.* from project as p left join project_has_person as pp ON pp.project_id=p.id left join person as ps ON pp.person_id=ps.id left join `function` as f ON pp.function_id=f.id;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            projectList = this.resultToModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    @Override
    public void save(Project project) {
        String query = null;

        if (project.getId() == null) {
            query = "INSERT INTO project (name) VALUES (?);";
        } else {
            query = "UPDATE project SET name=? WHERE id=?;";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setString(1, project.getName());
            if (project.getId() != null) {
                statement.setInt(2, project.getId());
            }

            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        String query = new String("DELETE FROM project WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addPerson(Project project, Integer personId, Integer functionId) {
        if (!this.personWithFunctionExists(project, personId, functionId)) {
            String query = "INSERT INTO project_has_person (project_id, person_id, function_id) VALUES (?, ?, ?);";

            try (
                    Connection conn = ConnectionManager.getConnection();
                    PreparedStatement statement = conn.prepareStatement(query)
            ) {
                statement.setInt(1, project.getId());
                statement.setInt(2, personId);
                statement.setInt(3, functionId);

                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deletePerson(Project project, Integer personId, Integer functionId) {
        if (this.personWithFunctionExists(project, personId, functionId)) {
            String query = new String("DELETE FROM project_has_person WHERE project_id=? AND person_id=? AND function_id=?;");

            try (
                    Connection conn = ConnectionManager.getConnection();
                    PreparedStatement statement = conn.prepareStatement(query)
            ) {
                statement.setInt(1, project.getId());
                statement.setInt(2, personId);
                statement.setInt(3, functionId);
                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Project> getProjectsByPersonId(Integer personId) {
        List<Project> projectList = new ArrayList<>();

        String query = new String("select p.id, p.name, ps.*, f.* from project as p left join project_has_person as pp ON pp.project_id=p.id left join person as ps ON pp.person_id=ps.id left join `function` as f ON pp.function_id=f.id WHERE pp.person_id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, personId);
            ResultSet rs = statement.executeQuery();
            projectList = this.resultToModel(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    private boolean personWithFunctionExists(Project project, Integer personId, Integer functionId) {
        String query = new String("SELECT * FROM project_has_person WHERE project_id=? AND person_id=? AND function_id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, project.getId());
            statement.setInt(2, personId);
            statement.setInt(3, functionId);
            ResultSet rs = statement.executeQuery();
            return (Boolean)rs.first();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean isPersonProjectLead(Integer projectId, Integer personId) {
        String query = new String("SELECT 1 FROM project_has_person WHERE person_id=? AND project_id=? AND function_id=1 LIMIT 1;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, personId);
            statement.setInt(2, projectId);
            ResultSet rs = statement.executeQuery();
            return (Boolean)rs.first();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    private List<Project> resultToModel(ResultSet rs) {
        HashMap<Integer, Project> projectList = new HashMap<>();

        try {
            while (rs.next()) {
                Project project = projectList.get(rs.getInt(1));
                if (project == null) {
                    projectList.put(rs.getInt(1), this.getModelFromResultSet(rs));
                    if (rs.getInt(3) != 0) {
                        project = projectList.get(rs.getInt(1));
                        project.addPerson(new Person(
                                rs.getInt(3),
                                rs.getString(4),
                                rs.getString(5),
                                null,
                                false,
                                new Function(
                                        rs.getInt(8),
                                        rs.getString(9)
                                )
                        ));
                    }
                } else {
                    project.addPerson(new Person(
                            rs.getInt(3),
                            rs.getString(4),
                            rs.getString(5),
                            null,
                            false,
                            new Function(
                                    rs.getInt(8),
                                    rs.getString(9)
                            )
                    ));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ArrayList<>(projectList.values());
    }

    private Project getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Project(
                rs.getInt(1),
                rs.getString(2)
        );
    }
}
