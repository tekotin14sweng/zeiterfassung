package ch.teko.dao.impl;

import ch.teko.dao.inter.TeamDAO;
import ch.teko.model.Person;
import ch.teko.model.Team;
import ch.teko.utils.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySqlTeamDAO implements TeamDAO {

    private static TeamDAO instance;

    public static TeamDAO getInstance() {
        if (MySqlTeamDAO.instance == null) MySqlTeamDAO.instance = new MySqlTeamDAO();
        return MySqlTeamDAO.instance;
    }

    @Override
    public List<Team> getAll() {
        List<Team> tempList = new ArrayList<>();

        String query = new String("select * from team inner join person on team.leader_id=person.id;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public void save(Team team, Integer leaderId) {
        String query = null;

        if (team.getId() == null) {
            query = "INSERT INTO team (name, leader_id) VALUES (?, ?);";
        } else {
            query = "UPDATE team SET name=?, leader_id=? WHERE id=?;";
        }

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setString(1, team.getName());
            statement.setInt(2, leaderId);
            if (team.getId() != null) {
                statement.setInt(3, team.getId());
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        String query = new String("DELETE FROM team WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isPersonTeamLead(Integer teamId, Integer personId) {
        String query = new String("SELECT 1 FROM team WHERE id=? AND leader_id=? LIMIT 1;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, teamId);
            statement.setInt(2, personId);
            ResultSet rs = statement.executeQuery();
            return (Boolean)rs.first();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    private Team getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Team(
                rs.getInt(1),
                rs.getString(2),
                new Person(
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        null,
                        false
                )
        );
    }
}
