package ch.teko.dao.impl;

import ch.teko.dao.inter.ReportingDAO;
import ch.teko.model.PdfBooking;
import ch.teko.model.Report;
import ch.teko.utils.ConnectionManager;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MySqlReportingDAO implements ReportingDAO {

    private static ReportingDAO instance;

    public static ReportingDAO getInstance() {
        if (MySqlReportingDAO.instance == null) MySqlReportingDAO.instance = new MySqlReportingDAO();
        return MySqlReportingDAO.instance;
    }

    @Override
    public List<Report> getAll() {
        List<Report> tempList = new ArrayList<>();

        String query = new String("select * from report;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(this.getModelFromResultSet(rs));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public List<String> getAllOpenMonths() {
        List<String> tempList = new ArrayList<>();

        String query = new String("SELECT DATE_FORMAT(bo.date_from, \"%m.%Y\") FROM booking AS bo " +
                "WHERE DATE_FORMAT(bo.date_from, \"%m.%Y\") NOT IN (SELECT DATE_FORMAT(`date`, \"%m.%Y\") FROM report WHERE revoked=FALSE) " +
                "GROUP BY DATE_FORMAT(bo.date_from, \"%m.%Y\");");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(rs.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public List<PdfBooking> getAllPdfBooking(Integer reportId) {
        List<PdfBooking> tempList = new ArrayList<>();

        String query = new String("SELECT * FROM vw_reporting WHERE report_id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, reportId);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                tempList.add(
                        new PdfBooking(
                                rs.getString(1),
                                rs.getString(2),
                                rs.getString(3),
                                rs.getString(4),
                                rs.getString(5),
                                rs.getString(6),
                                rs.getTimestamp(7).toLocalDateTime(),
                                rs.getTimestamp(8).toLocalDateTime()
                        )
                );
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tempList;
    }

    @Override
    public void revoke(Integer reportId) {
        String query = new String("UPDATE report SET revoked=true, revoked_date=now() WHERE id=?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, reportId);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void archiveMonth(String month) {

        Integer reportId = this.createReport(month);

        if (reportId != null) {
            this.archiveBookings(month, reportId);
        }
    }

    private Integer createReport(String month) {
        String query = new String("INSERT INTO report (date) VALUES (?);");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)
        ) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            LocalDate dt = LocalDate.parse("01." + month, formatter);
            statement.setDate(1, java.sql.Date.valueOf(dt));
            return statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void archiveBookings(String month, Integer reportId) {
        String query = new String("INSERT INTO report_booking (person_id, project_id, time_account_id, activity_id, date_from, date_to, report_id) " +
                "SELECT person_id, project_id, time_account_id, activity_id, date_from, date_to, ? FROM booking AS bo WHERE DATE_FORMAT(bo.date_from, \"%m.%Y\") = ?;");

        try (
                Connection conn = ConnectionManager.getConnection();
                PreparedStatement statement = conn.prepareStatement(query)
        ) {
            statement.setInt(1, reportId);
            statement.setString(2, month);
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Report getModelFromResultSet(ResultSet rs) throws SQLException {
        return new Report(
                rs.getInt(1),
                rs.getDate(2).toLocalDate(),
                rs.getBoolean(3),
                rs.getDate(4) != null ? rs.getDate(4).toLocalDate() : null
        );
    }
}
