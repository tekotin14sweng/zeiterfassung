package ch.teko.dao.inter;

import ch.teko.model.Function;

import java.util.List;

public interface FunctionDAO {
	public List<Function> getAll();
}
