package ch.teko.dao.inter;

import ch.teko.model.PdfBooking;
import ch.teko.model.Report;

import java.util.List;

public interface ReportingDAO {
	public List<Report> getAll();

	public List<String> getAllOpenMonths();

	public List<PdfBooking> getAllPdfBooking(Integer reportId);

	public void revoke(Integer reportId);

	public void archiveMonth(String month);
}
