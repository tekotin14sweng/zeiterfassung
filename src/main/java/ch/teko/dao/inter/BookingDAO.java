package ch.teko.dao.inter;

import ch.teko.model.Booking;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingDAO {
	public List<Booking> getAll();

	public List<Booking> getAllByPerson(Integer personId);

	public void save(Booking booking);

	public void delete(Integer id);

	public boolean isMonthBookable(LocalDateTime date);
}
