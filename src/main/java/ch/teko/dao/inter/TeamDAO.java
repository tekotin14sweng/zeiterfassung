package ch.teko.dao.inter;

import ch.teko.model.Team;

import java.util.List;

public interface TeamDAO {
	public List<Team> getAll();

	public void save(Team team, Integer leaderId);

	public void delete(Integer id);

	public boolean isPersonTeamLead(Integer teamId, Integer personId);
}
