package ch.teko.dao.inter;

import ch.teko.model.Activity;

import java.util.List;

public interface ActivityDAO {
	public List<Activity> getAll();
}
