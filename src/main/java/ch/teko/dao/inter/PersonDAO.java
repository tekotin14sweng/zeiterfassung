package ch.teko.dao.inter;

import ch.teko.model.Person;

import java.util.List;

public interface PersonDAO {
	public List<Person> getAll();

	public void save(Person person, Integer teamId);

	public void delete(Integer id);
}
