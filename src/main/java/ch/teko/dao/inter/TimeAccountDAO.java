package ch.teko.dao.inter;

import ch.teko.model.TimeAccount;

import java.util.List;

public interface TimeAccountDAO {
	public List<TimeAccount> getAll();

	public void save(TimeAccount timeAccount);

	public void delete(Integer id);

	public void addProject(TimeAccount timeAccount, Integer projectId);

	public void deleteProject(TimeAccount timeAccount, Integer projectId);

	public List<TimeAccount> getTimeAccountByProjectId(Integer projectId);
}
