package ch.teko.dao.inter;

import ch.teko.model.Project;

import java.util.List;

public interface ProjectDAO {
	public List<Project> getAll();

	public void save(Project project);

	public void delete(Integer id);

	public void addPerson(Project project, Integer personId, Integer functionId);

	public void deletePerson(Project project, Integer personId, Integer functionId);

	public List<Project> getProjectsByPersonId(Integer personId);

	public boolean isPersonProjectLead(Integer projectId, Integer personId);
}
