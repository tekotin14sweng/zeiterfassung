package ch.teko.controller;

import ch.teko.dao.impl.MySqlPersonDAO;
import ch.teko.model.Person;
import ch.teko.utils.UserManager;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;

public class EntryController {

	@FXML
	private ComboBox<Person> dropPerson;
	@FXML
	private Button btnPerson;
	@FXML
	private Button btnTeam;
	@FXML
	private Button btnProject;
	@FXML
	private Button btnTimeAccount;
	@FXML
	private Button btnBooking;
	@FXML
	private Button btnReporting;

	public void initialize() {
		this.btnPerson.setOnAction((event) -> this.showDialog("person.fxml"));
		this.btnTeam.setOnAction((event) -> this.showDialog("team.fxml"));
		this.btnProject.setOnAction((event) -> this.showDialog("project.fxml"));
		this.btnTimeAccount.setOnAction((event) -> this.showDialog("timeAccount.fxml"));
		this.btnBooking.setOnAction((event) -> this.showDialog("booking.fxml"));
		this.btnReporting.setOnAction((event) -> this.showDialog("reporting.fxml"));

		this.loadPerson();

		this.dropPerson.setOnAction((event) -> {
			Person selectedPerson = this.dropPerson.getSelectionModel().getSelectedItem();
			if (selectedPerson != null) {
				UserManager.setCurrentPerson(selectedPerson);
				this.evaluateRight();
			}
		});
		this.dropPerson.getSelectionModel().selectFirst();
		UserManager.setCurrentPerson(this.dropPerson.getSelectionModel().getSelectedItem());
		this.evaluateRight();
	}

	private void showDialog(String stagePath) {

		try {
			Dialog<ButtonType> dialog = new Dialog<>();
			FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource(stagePath));
			try {
				dialog.getDialogPane().setContent(loader.load());
			} catch (Exception e) {
				e.printStackTrace();
			}
			dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
			Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
			closeButton.managedProperty().bind(closeButton.visibleProperty());
			dialog.showAndWait();
			this.loadPerson();
		}
		catch (Exception $e) {
			$e.printStackTrace();
		}
	}

	private void evaluateRight() {
		if (!UserManager.isCurrentUserAdmin()) {
			this.btnTimeAccount.setDisable(true);
			this.btnReporting.setDisable(true);
		} else {
			this.btnTimeAccount.setDisable(false);
			this.btnReporting.setDisable(false);
		}
	}

	private void loadPerson() {
		Person selectedPerson = this.dropPerson.getSelectionModel().getSelectedItem();
		this.dropPerson.getItems().clear();
		this.dropPerson.getItems().addAll(MySqlPersonDAO.getInstance().getAll());

		if (selectedPerson != null) {
			this.dropPerson.getSelectionModel().select(selectedPerson);
		}
	}
}
