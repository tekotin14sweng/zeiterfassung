package ch.teko.controller;

import ch.teko.dao.impl.MySqlPersonDAO;
import ch.teko.dao.impl.MySqlTeamDAO;
import ch.teko.model.Person;
import ch.teko.model.Team;
import ch.teko.utils.Toast;
import ch.teko.utils.UserManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class TeamController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnReset;
	@FXML
	private TextField inputName;
	@FXML
	private ComboBox<Person> dropLead;
	@FXML
	private TableView<Team> tableTeam;
	@FXML
	private TableColumn<Team, String> columnName;
	@FXML
	private TableColumn<Team, String> columnLead;

	private Team selectedTeam = new Team();

	public void initialize() {
		// Load all Teams
		this.loadTeams();

		// Setup table
		this.columnName.setCellValueFactory(team -> new SimpleStringProperty(team.getValue().getName()));
		this.columnLead.setCellValueFactory(team -> new SimpleStringProperty(team.getValue().getLeaderName()));

		// Setup combobox
		this.dropLead.getItems().addAll(MySqlPersonDAO.getInstance().getAll());

		// Listen for table row selections
		this.tableTeam.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				Team teamClicked = this.tableTeam.getSelectionModel().getSelectedItem();
				if (teamClicked != null && UserManager.isCurrentUserAdmin()) {
					this.selectedTeam = teamClicked;
					this.editSelected();
				}
			}
		});

		// Button listeners / inital state
		this.addMode();
		this.btnReset.setOnAction((event) -> this.addMode());
		this.btnSave.setOnAction((event) -> this.saveTeam());
		this.btnDelete.setOnAction((event) -> {
			MySqlTeamDAO.getInstance().delete(this.selectedTeam.getId());
			this.loadTeams();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.saveTeam());
	}

	private void saveTeam() {
		if (this.inputName.getText().length() == 0 || this.dropLead.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.inputName.getScene().getWindow(), "Bitte wähle ein Name \nund ein Teamleiter.");
		} else {
			this.selectedTeam.setName(this.inputName.getText());
			MySqlTeamDAO.getInstance().save(this.selectedTeam, this.dropLead.getSelectionModel().getSelectedItem().getId());
			this.loadTeams();
			this.addMode();
		}
	}

	private void loadTeams() {
		this.tableTeam.getItems().clear();
		this.tableTeam.getItems().addAll(MySqlTeamDAO.getInstance().getAll());
	}

	private void editSelected() {
		this.inputName.setText(this.selectedTeam.getName());
		this.selectDropLead();
		this.editMode();
	}

	private void selectDropLead() {
		this.dropLead.setValue(this.selectedTeam.getLeader());
	}

	private void editMode() {
		this.btnDelete.setDisable(false);
		this.btnSave.setDisable(false);
		this.btnAdd.setDisable(true);
	}

	private void addMode() {
		this.selectedTeam = new Team();
		this.inputName.clear();
		this.dropLead.getSelectionModel().clearSelection();
		if (UserManager.isCurrentUserAdmin()) {
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(false);
		} else {
			this.inputName.setDisable(true);
			this.dropLead.setDisable(true);
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(true);
			this.btnReset.setDisable(true);
		}
	}
}
