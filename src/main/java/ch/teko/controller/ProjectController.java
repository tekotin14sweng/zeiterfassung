package ch.teko.controller;

import ch.teko.dao.impl.MySqlFunctionDAO;
import ch.teko.dao.impl.MySqlPersonDAO;
import ch.teko.dao.impl.MySqlProjectDAO;
import ch.teko.model.Function;
import ch.teko.model.Person;
import ch.teko.model.Project;
import ch.teko.utils.Toast;
import ch.teko.utils.UserManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ProjectController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnReset;
	@FXML
	private Button btnPersonAdd;
	@FXML
	private Button btnPersonDelete;
	@FXML
	private TextField inputName;
	@FXML
	private Label labelProject;
	@FXML
	private ComboBox<Person> dropPerson;
	@FXML
	private ComboBox<Function> dropFunction;
	@FXML
	private TableView<Project> tableProject;
	@FXML
	private TableColumn<Project, String> columnName;
	@FXML
	private TableColumn<Project, String> columnMembers;

	private Project selectedProject = new Project();

	public void initialize() {
		// Load all Teams
		this.loadProjects();

		// Setup table
		this.columnName.setCellValueFactory(project -> new SimpleStringProperty(project.getValue().getName()));
		this.columnMembers.setCellValueFactory(project -> new SimpleStringProperty(project.getValue().getProjectMembers()));
		this.columnMembers.setCellFactory(tv -> new TableCell<Project, String>() {

			private final VBox lines; {
				lines = new VBox();
				setGraphic(lines);
			}

			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);

				lines.getChildren().clear();

				if (!empty && item != null) {
					int lineNo = 1;
					for (String line : item.split("\n")) {
						Text text = new Text(line);
						text.getStyleClass().add("line-" + (lineNo++));
						lines.getChildren().add(text);
					}
				}
			}

		});

		// Setup combobox
		this.dropPerson.getItems().addAll(MySqlPersonDAO.getInstance().getAll());
		this.dropFunction.getItems().addAll(MySqlFunctionDAO.getInstance().getAll());

		// Listen for table row selections
		this.tableProject.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				Project projectClicked = this.tableProject.getSelectionModel().getSelectedItem();
				if (projectClicked != null && (UserManager.isCurrentUserAdmin() || UserManager.isProjectLead(projectClicked.getId()))) {
					this.selectedProject = projectClicked;
					this.editSelected();
				} else {
					this.addMode();
				}
			}
		});

		// Button listeners / initial state
		this.addMode();
		this.btnReset.setOnAction((event) -> this.addMode());
		this.btnSave.setOnAction((event) -> this.saveProject());
		this.btnDelete.setOnAction((event) -> {
			MySqlProjectDAO.getInstance().delete(this.selectedProject.getId());
			this.loadProjects();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.saveProject());
		this.btnPersonAdd.setOnAction((event) -> this.addPersonToProject());
		this.btnPersonDelete.setOnAction((event) -> this.deletePersonFromProject());
	}

	private void addPersonToProject() {
		if (this.dropPerson.getSelectionModel().getSelectedItem() == null || this.dropFunction.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropPerson.getScene().getWindow(), "Bitte Person und Funktion\nauswählen");
		} else {
			MySqlProjectDAO.getInstance().addPerson(this.selectedProject, this.dropPerson.getSelectionModel().getSelectedItem().getId(), this.dropFunction.getSelectionModel().getSelectedItem().getId());
			this.loadProjects();
		}
	}

	private void deletePersonFromProject() {
		if (this.dropPerson.getSelectionModel().getSelectedItem() == null || this.dropFunction.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropPerson.getScene().getWindow(), "Bitte Person und Funktion\nauswählen");
		} else {
			MySqlProjectDAO.getInstance().deletePerson(this.selectedProject, this.dropPerson.getSelectionModel().getSelectedItem().getId(), this.dropFunction.getSelectionModel().getSelectedItem().getId());
			this.loadProjects();
		}
	}

	private void saveProject() {
		if (this.inputName.getText().length() == 0) {
			Toast.makeTextError((Stage) this.inputName.getScene().getWindow(), "Bitte Projektname ausfüllen");
		} else {
			this.selectedProject.setName(this.inputName.getText());
			MySqlProjectDAO.getInstance().save(this.selectedProject);
			this.loadProjects();
			this.addMode();
		}
	}

	private void loadProjects() {
		this.tableProject.getItems().clear();
		this.tableProject.getItems().addAll(MySqlProjectDAO.getInstance().getAll());
	}

	private void editSelected() {
		this.inputName.setText(this.selectedProject.getName());
		this.editMode();
	}

	private void editMode() {
		if (UserManager.isCurrentUserAdmin()) {
			this.btnDelete.setDisable(false);
			this.btnSave.setDisable(false);
			this.btnAdd.setDisable(true);
		}
		this.labelProject.setText(this.selectedProject.getName());
		if (UserManager.isCurrentUserAdmin() || UserManager.isProjectLead(this.selectedProject.getId())) {
			this.btnPersonAdd.setDisable(false);
			this.btnPersonDelete.setDisable(false);
			this.dropFunction.setDisable(false);
			this.dropPerson.setDisable(false);
		}
	}

	private void addMode() {
		this.selectedProject = new Project();
		this.inputName.clear();
		this.labelProject.setText("Projekt wählen...");
		this.dropPerson.getSelectionModel().clearSelection();
		this.dropFunction.getSelectionModel().clearSelection();
		this.dropFunction.setDisable(true);
		this.dropPerson.setDisable(true);
		this.btnPersonAdd.setDisable(true);
		this.btnPersonDelete.setDisable(true);
		if (UserManager.isCurrentUserAdmin()) {
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(false);
		} else {
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(true);
			this.btnReset.setDisable(true);
			this.inputName.setDisable(true);
		}
	}
}
