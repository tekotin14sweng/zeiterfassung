package ch.teko.controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {

		// Load first view
		FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("entry.fxml"));

		// Initialize first scene
		primaryStage.setScene(new Scene(loader.load()));
		primaryStage.setTitle("Zeiterfassung");
		primaryStage.show();
	}

	public static void main(String[] args) throws Exception {
		launch(args);
	}

}
