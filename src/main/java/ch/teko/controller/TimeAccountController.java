package ch.teko.controller;

import ch.teko.dao.impl.MySqlProjectDAO;
import ch.teko.dao.impl.MySqlTimeAccountDAO;
import ch.teko.model.Project;
import ch.teko.model.TimeAccount;
import ch.teko.utils.Toast;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class TimeAccountController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnReset;
	@FXML
	private Button btnProjectAdd;
	@FXML
	private Button btnProjectDelete;
	@FXML
	private TextField inputName;
	@FXML
	private TextField inputHours;
	@FXML
	private Label labelTimeAccount;
	@FXML
	private ComboBox<Project> dropProject;
	@FXML
	private TableView<TimeAccount> tableTimeAccount;
	@FXML
	private TableColumn<TimeAccount, String> columnTimeAccount;;
	@FXML
	private TableColumn<TimeAccount, String> columnHours;
	@FXML
	private TableColumn<TimeAccount, String> columnProject;

	private TimeAccount selectedTimeAccount = new TimeAccount();

	public void initialize() {
		// Load all Teams
		this.loadTimeAccounts();

		// Setup table
		this.columnTimeAccount.setCellValueFactory(timeAccount -> new SimpleStringProperty(timeAccount.getValue().getName()));
		this.columnHours.setCellValueFactory(timeAccount -> new SimpleStringProperty(timeAccount.getValue().getMaxHours().toString()));
		this.columnProject.setCellValueFactory(timeAccount -> new SimpleStringProperty(timeAccount.getValue().getProjects()));
		this.columnProject.setCellFactory(tv -> new TableCell<TimeAccount, String>() {
			private final VBox lines; {
				lines = new VBox();
				setGraphic(lines);
			}
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);

				lines.getChildren().clear();

				if (!empty && item != null) {
					int lineNo = 1;
					for (String line : item.split("\n")) {
						Text text = new Text(line);
						text.getStyleClass().add("line-" + (lineNo++));
						lines.getChildren().add(text);
					}
				}
			}

		});

		// Setup combobox
		this.dropProject.getItems().addAll(MySqlProjectDAO.getInstance().getAll());

		// Listen for table row selections
		this.tableTimeAccount.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				TimeAccount timeAccountClicked = this.tableTimeAccount.getSelectionModel().getSelectedItem();
				if (timeAccountClicked != null) {
					this.selectedTimeAccount = timeAccountClicked;
					this.editSelected();
				}
			}
		});

		// Button listeners / initial state
		this.addMode();
		this.btnReset.setOnAction((event) -> this.addMode());
		this.btnSave.setOnAction((event) -> this.saveTimeAccount());
		this.btnDelete.setOnAction((event) -> {
			MySqlTimeAccountDAO.getInstance().delete(this.selectedTimeAccount.getId());
			this.loadTimeAccounts();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.saveTimeAccount());
		this.btnProjectAdd.setOnAction((event) -> this.addProjectToTimeAccount());
		this.btnProjectDelete.setOnAction((event) -> this.deleteProjectFromTimeAccount());
	}

	private void addProjectToTimeAccount() {
		if (this.dropProject.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropProject.getScene().getWindow(), "Bitte Projekt wählen");
		} else {
			MySqlTimeAccountDAO.getInstance().addProject(this.selectedTimeAccount, this.dropProject.getSelectionModel().getSelectedItem().getId());
			this.loadTimeAccounts();
		}
	}

	private void deleteProjectFromTimeAccount() {
		if (this.dropProject.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropProject.getScene().getWindow(), "Bitte Projekt wählen");
		} else {
			MySqlTimeAccountDAO.getInstance().deleteProject(this.selectedTimeAccount, this.dropProject.getSelectionModel().getSelectedItem().getId());
			this.loadTimeAccounts();
		}
	}

	private void saveTimeAccount() {
		if (this.inputName.getText().length() == 0) {
			Toast.makeTextError((Stage) this.inputName.getScene().getWindow(), "Bitte Zeitkontoname ausfüllen");
		} else {
			this.selectedTimeAccount.setName(this.inputName.getText());
			this.selectedTimeAccount.setMaxHours(Integer.parseInt(this.inputHours.getText()));
			MySqlTimeAccountDAO.getInstance().save(this.selectedTimeAccount);
			this.loadTimeAccounts();
			this.addMode();
		}
	}

	private void loadTimeAccounts() {
		this.tableTimeAccount.getItems().clear();
		this.tableTimeAccount.getItems().addAll(MySqlTimeAccountDAO.getInstance().getAll());
	}

	private void editSelected() {
		this.inputName.setText(this.selectedTimeAccount.getName());
		this.inputHours.setText(this.selectedTimeAccount.getMaxHours().toString());
		this.editMode();
	}

	private void editMode() {
		this.btnDelete.setDisable(false);
		this.btnSave.setDisable(false);
		this.btnAdd.setDisable(true);
		this.labelTimeAccount.setText(this.selectedTimeAccount.getName());
		this.btnProjectAdd.setDisable(false);
		this.btnProjectDelete.setDisable(false);
	}

	private void addMode() {
		this.selectedTimeAccount = new TimeAccount();
		this.inputName.clear();
		this.inputHours.clear();
		this.labelTimeAccount.setText("Zeitkonto wählen...");
		this.dropProject.getSelectionModel().clearSelection();
		this.btnDelete.setDisable(true);
		this.btnSave.setDisable(true);
		this.btnAdd.setDisable(false);
		this.btnProjectAdd.setDisable(true);
		this.btnProjectDelete.setDisable(true);
	}
}
