package ch.teko.controller;

import ch.teko.dao.impl.MySqlPersonDAO;
import ch.teko.dao.impl.MySqlTeamDAO;
import ch.teko.model.Person;
import ch.teko.model.Team;
import ch.teko.utils.Toast;
import ch.teko.utils.UserManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class PersonController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnReset;
	@FXML
	private TextField inputFirstname;
	@FXML
	private TextField inputLastname;
	@FXML
	private ComboBox<Team> dropTeam;
	@FXML
	private TableView<Person> tablePerson;
	@FXML
	private TableColumn<Person, String> columnPerson;
	@FXML
	private TableColumn<Person, String> columnTeam;

	private Person selectedPerson = new Person();

	public void initialize() {
		// Load all Teams
		this.loadPersons();

		// Setup table
		this.columnPerson.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getFullname()));
		this.columnTeam.setCellValueFactory(person -> new SimpleStringProperty(person.getValue().getTeam().getName()));

		// Setup combobox
		this.dropTeam.getItems().addAll(MySqlTeamDAO.getInstance().getAll());

		// Listen for table row selections
		this.tablePerson.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				Person personClicked = this.tablePerson.getSelectionModel().getSelectedItem();
				if (personClicked != null && (personClicked.getId() == UserManager.getCurrentPerson().getId() || UserManager.isCurrentUserAdmin() || UserManager.isTeamLead(personClicked.getTeam().getId()))) {
					this.selectedPerson = personClicked;
					this.editSelected();
				} else {
					this.addMode();
				}
			}
		});

		// Button listeners / inital state
		this.addMode();
		this.btnReset.setOnAction((event) -> this.addMode());
		this.btnSave.setOnAction((event) -> this.savePerson());
		this.btnDelete.setOnAction((event) -> {
			MySqlPersonDAO.getInstance().delete(selectedPerson.getId());
			this.loadPersons();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.savePerson());
	}

	private void savePerson() {
		// Validate
		if (this.inputFirstname.getText().length() == 0 || this.inputLastname.getText().length() == 0){
			Toast.makeTextError((Stage) this.inputFirstname.getScene().getWindow(), "Bitte Vor- und Nachname\nausfüllen");
		} else {
			this.selectedPerson.setFirstname(this.inputFirstname.getText());
			this.selectedPerson.setLastname(this.inputLastname.getText());
			Integer teamId = this.dropTeam.getSelectionModel().getSelectedItem() == null ? 0 : this.dropTeam.getSelectionModel().getSelectedItem().getId();
			MySqlPersonDAO.getInstance().save(this.selectedPerson, teamId);
			this.loadPersons();
			this.addMode();
		}
	}

	private void loadPersons() {
		this.tablePerson.getItems().clear();
		this.tablePerson.getItems().addAll(MySqlPersonDAO.getInstance().getAll());
	}

	private void editSelected() {
		this.inputFirstname.setText(this.selectedPerson.getFirstname());
		this.inputLastname.setText(this.selectedPerson.getLastname());
		this.selectDropLead();
		this.editMode();
	}

	private void selectDropLead() {
		this.dropTeam.setValue(this.selectedPerson.getTeam());
	}

	private void editMode() {
		this.btnDelete.setDisable(false);
		this.btnSave.setDisable(false);
		this.btnAdd.setDisable(true);
		this.inputFirstname.setDisable(false);
		this.inputLastname.setDisable(false);
	}

	private void addMode() {
		this.selectedPerson = new Person();
		this.inputFirstname.clear();
		this.inputLastname.clear();
		this.dropTeam.getSelectionModel().clearSelection();
		if (UserManager.isCurrentUserAdmin()) {
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(false);
		} else {
			this.inputFirstname.setDisable(true);
			this.inputLastname.setDisable(true);
			this.dropTeam.setDisable(true);
			this.btnDelete.setDisable(true);
			this.btnSave.setDisable(true);
			this.btnAdd.setDisable(true);
		}
	}
}
