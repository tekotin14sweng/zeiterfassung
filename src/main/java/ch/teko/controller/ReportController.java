package ch.teko.controller;

import ch.teko.dao.impl.MySqlReportingDAO;
import ch.teko.model.Report;
import ch.teko.utils.PdfGenerator;
import ch.teko.utils.Toast;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class ReportController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnDownload;
	@FXML
	private Button btnReset;
	@FXML
	private Label labelMonth;
	@FXML
	private Label labelCurrentMonth;
	@FXML
	private ComboBox<String> dropMonth;
	@FXML
	private TableView<Report> tableReport;
	@FXML
	private TableColumn<Report, String> columnMonth;
	@FXML
	private TableColumn<Report, String> columnRevoked;

	private Report selectedReport = new Report();

	private String pdfPath = new String();

	public void initialize() {
		// Load all Teams
		this.loadReports();

		// Setup table
		this.columnMonth.setCellValueFactory(report -> new SimpleStringProperty(report.getValue().toString()));
		this.columnRevoked.setCellValueFactory(report -> new SimpleStringProperty(report.getValue().getRevokedDescription()));

		// Setup combobox
		this.loadMonths();

		// Listen for table row selections
		this.tableReport.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				Report reportClicked = this.tableReport.getSelectionModel().getSelectedItem();
				if (reportClicked != null) {
					this.selectedReport = reportClicked;
					this.editSelected();
				}
			}
		});

		// Button listeners / inital state
		this.addMode();
		this.btnDelete.setOnAction((event) -> {
			MySqlReportingDAO.getInstance().revoke(selectedReport.getId());
			this.loadReports();
			this.loadMonths();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.savePerson());
		this.btnReset.setOnAction((event) -> this.addMode());
		this.btnDownload.setOnAction((event) -> {
			this.chooseFile();
			this.generatePdf();
		});
	}

	private void savePerson() {
		if (this.dropMonth.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropMonth.getScene().getWindow(), "Bitte wähle ein Monat aus");
		} else {
			MySqlReportingDAO.getInstance().archiveMonth(this.dropMonth.getSelectionModel().getSelectedItem());
			this.loadReports();
			this.addMode();
		}
	}

	private void loadReports() {
		this.tableReport.getItems().clear();
		this.tableReport.getItems().addAll(MySqlReportingDAO.getInstance().getAll());
	}

	private void loadMonths() {
		this.dropMonth.getItems().clear();
		this.dropMonth.getItems().addAll(MySqlReportingDAO.getInstance().getAllOpenMonths());
	}

	private void editSelected() {

		this.editMode();
	}

	private void editMode() {
		this.btnDelete.setDisable(false);
		this.btnDownload.setDisable(false);
		this.btnAdd.setDisable(true);
		this.dropMonth.setDisable(true);
		this.labelMonth.setVisible(true);
		this.labelCurrentMonth.setText(this.selectedReport.toString());
		this.labelCurrentMonth.setVisible(true);
	}

	private void addMode() {
		this.selectedReport = new Report();
		this.dropMonth.getSelectionModel().clearSelection();
		this.dropMonth.setDisable(false);
		this.btnDelete.setDisable(true);
		this.btnDownload.setDisable(true);
		this.btnAdd.setDisable(false);
		this.labelMonth.setVisible(false);
		this.labelCurrentMonth.setVisible(false);
	}

	private void chooseFile() {
		FileChooser fileChooser = new FileChooser();

		//Set extension filter
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PDF files (*.pdf)", "*.pdf");
		fileChooser.getExtensionFilters().add(extFilter);
		fileChooser.setTitle("Report: " + this.selectedReport.toString());
		fileChooser.setInitialFileName("report-" + this.selectedReport.toString());

		//Show save file dialog
		File file = fileChooser.showSaveDialog(this.btnDownload.getScene().getWindow());

		if(file != null){
			this.pdfPath = file.getAbsolutePath();
		}
	}

	private void generatePdf() {
		PdfGenerator.generatePdf(
				this.pdfPath,
				this.selectedReport
		);
	}
}
