package ch.teko.controller;

import ch.teko.dao.impl.*;
import ch.teko.model.*;
import ch.teko.utils.Toast;
import ch.teko.utils.UserManager;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import jfxtras.scene.control.LocalDateTimeTextField;

import java.time.LocalDateTime;

public class BookingController {

	@FXML
	private Button btnAdd;
	@FXML
	private Button btnSave;
	@FXML
	private Button btnDelete;
	@FXML
	private Button btnReset;
	@FXML
	private LocalDateTimeTextField datepickerFrom;
	@FXML
	private LocalDateTimeTextField datepickerTo;
	@FXML
	private ComboBox<Person> dropPerson;
	@FXML
	private ComboBox<Project> dropProject;
	@FXML
	private ComboBox<TimeAccount> dropTimeAccount;
	@FXML
	private ComboBox<Activity> dropActivity;
	@FXML
	private TableView<Booking> tableBooking;
	@FXML
	private TableColumn<Booking, String> columnName;
	@FXML
	private TableColumn<Booking, String> columnStatus;

	private Booking selectedBooking = new Booking();

	public void initialize() {
		// Setup table
		this.columnName.setCellValueFactory(booking -> new SimpleStringProperty(booking.getValue().getBookingName()));
		this.columnStatus.setCellValueFactory(booking -> new SimpleStringProperty(booking.getValue().getArchived() ? booking.getValue().getArchivedText() : ""));

		// Setup combobox
		this.dropPerson.getItems().addAll(MySqlPersonDAO.getInstance().getAll());
		this.dropActivity.getItems().addAll(MySqlActivityDAO.getInstance().getAll());

		// Listen for table row selections
		this.tableBooking.setOnMouseClicked((MouseEvent event) -> {
			if(event.getButton().equals(MouseButton.PRIMARY)){
				Booking bookingClicked = this.tableBooking.getSelectionModel().getSelectedItem();
				if (bookingClicked != null && bookingClicked.getArchived() == false && (UserManager.isCurrentUserAdmin() || bookingClicked.getPerson().getId() == UserManager.getCurrentPerson().getId())) {
					this.selectedBooking = bookingClicked;
					this.editSelected();
				} else {
					this.addMode();
				}
			}
		});

		// Button listeners / initial state
		this.addMode();
		this.btnReset.setOnAction((event) -> {
			this.addMode();
			this.loadBookings();
		});
		this.btnSave.setOnAction((event) -> this.saveBooking());
		this.btnDelete.setOnAction((event) -> {
			MySqlBookingDAO.getInstance().delete(selectedBooking.getId());
			this.loadBookings();
			this.addMode();
		});
		this.btnAdd.setOnAction((event) -> this.saveBooking());

		// Combobox change listeners
		this.dropPerson.setOnAction((event) -> {
			Person selectedPerson = this.dropPerson.getSelectionModel().getSelectedItem();
			if (selectedPerson != null) {
				this.loadProjects(selectedPerson.getId());
			}
		});
		this.dropProject.setOnAction((event) -> {
			Project selectedProject = this.dropProject.getSelectionModel().getSelectedItem();
			if (selectedProject != null) {
				this.loadTimeAccount(selectedProject.getId());
			}
		});
		this.dropPerson.getSelectionModel().select(UserManager.getCurrentPerson());
		this.loadProjects(this.dropPerson.getSelectionModel().getSelectedItem().getId());

		if (!UserManager.isCurrentUserAdmin()) {
			this.dropPerson.setDisable(true);
		}

		// Load all Teams
		this.loadBookings();
	}

	private void saveBooking() {
		if (this.dropPerson.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropPerson.getScene().getWindow(), "Bitte wähle eine Person");
		} else if (this.dropProject.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropProject.getScene().getWindow(), "Bitte wähle ein Projekt");
		} else if (this.dropTimeAccount.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropTimeAccount.getScene().getWindow(), "Bitte wähle ein Zeitkonto");
		} else if (this.dropActivity.getSelectionModel().getSelectedItem() == null) {
			Toast.makeTextError((Stage) this.dropActivity.getScene().getWindow(), "Bitte wähle eine Leistungsart");
		} else if (this.datepickerFrom.getLocalDateTime().isAfter(this.datepickerTo.getLocalDateTime())) {
			Toast.makeTextError((Stage) this.datepickerFrom.getScene().getWindow(), "Das Startdatum muss vor\ndem Enddatum sein");
		} else if (!MySqlBookingDAO.getInstance().isMonthBookable(this.datepickerFrom.getLocalDateTime())) {
			Toast.makeTextError((Stage) this.datepickerFrom.getScene().getWindow(), "Auf diesen Monat kann nicht mehr gebucht werden");
		} else {
			this.selectedBooking.setDateFrom(this.datepickerFrom.getLocalDateTime());
			this.selectedBooking.setDateTo(this.datepickerTo.getLocalDateTime());
			this.selectedBooking.setPerson(this.dropPerson.getSelectionModel().getSelectedItem());
			this.selectedBooking.setProject(this.dropProject.getSelectionModel().getSelectedItem());
			this.selectedBooking.setTimeAccount(this.dropTimeAccount.getSelectionModel().getSelectedItem());
			this.selectedBooking.setActivity(this.dropActivity.getSelectionModel().getSelectedItem());
			MySqlBookingDAO.getInstance().save(this.selectedBooking);
			this.loadBookings();
			this.addMode();
		}
	}

	private void loadBookings() {
		this.tableBooking.getItems().clear();
		Person selectedPerson = this.dropPerson.getSelectionModel().getSelectedItem();
		if (selectedPerson != null) {
			this.tableBooking.getItems().addAll(MySqlBookingDAO.getInstance().getAllByPerson(selectedPerson.getId()));
		} else {
			this.tableBooking.getItems().addAll(MySqlBookingDAO.getInstance().getAll());
		}
	}

	private void editSelected() {
		this.datepickerFrom.setLocalDateTime(this.selectedBooking.getDateFrom());
		this.datepickerTo.setLocalDateTime(this.selectedBooking.getDateTo());
		this.dropPerson.setValue(this.selectedBooking.getPerson());
		this.dropProject.setValue(this.selectedBooking.getProject());
		this.dropTimeAccount.setValue(this.selectedBooking.getTimeAccount());
		this.dropActivity.setValue(this.selectedBooking.getActivity());
		this.editMode();
	}

	private void editMode() {
		this.btnDelete.setDisable(false);
		this.btnSave.setDisable(false);
		this.btnAdd.setDisable(true);
	}

	private void addMode() {
		this.selectedBooking = new Booking();
		this.datepickerFrom.setLocalDateTime(LocalDateTime.now());
		this.datepickerTo.setLocalDateTime(LocalDateTime.now());
		if (UserManager.isCurrentUserAdmin()) {
			this.dropPerson.getSelectionModel().clearSelection();
		}
		this.dropProject.getSelectionModel().clearSelection();
		this.dropTimeAccount.getSelectionModel().clearSelection();
		this.dropActivity.getSelectionModel().clearSelection();
		this.btnDelete.setDisable(true);
		this.btnSave.setDisable(true);
		this.btnAdd.setDisable(false);
	}

	private void loadProjects(Integer personId) {
		this.dropProject.getItems().clear();
		this.dropProject.getItems().addAll(MySqlProjectDAO.getInstance().getProjectsByPersonId(personId));
	}

	private void loadTimeAccount(Integer projectId) {
		this.dropTimeAccount.getItems().clear();
		this.dropTimeAccount.getItems().addAll(MySqlTimeAccountDAO.getInstance().getTimeAccountByProjectId(projectId));
	}
}
